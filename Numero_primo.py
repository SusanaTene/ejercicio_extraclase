print("-+-+-+-+-+-ACTIVIDAD EXTRACLASE-+-+-+-+-+-")
print("Autora: Lilia Susana Tene.")
print("Email: lilia.tene@unl.edu.ec")
print("Fecha: 23 de mayo del 2020")

#Determinar si un número es primo o no;

def es_primo(numero):

    if numero <= 1:
        return False
    elif numero == 2:
        return True
    else:
        for i in range(2, numero):
            if numero % i == 0:
                return False
        return True

def primo():

    numero = int(input('Inserta un numero:'))
    total = es_primo(numero)

    if total is True:
        print('El número SI es primo')
    else:
        print('El número NO es primo')

if __name__ == '__main__':
    primo()